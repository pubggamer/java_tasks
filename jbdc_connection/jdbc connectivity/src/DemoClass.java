//1.import
    /*
    1.import ---> java.sql
    2.load and register the driver --> com.mysql.jdbc.Driver
    3. create a connection --> Conncection
    4. create a statement --> Statement

    5. execute the query
    6. process the results
    7. close
     */
import java.sql.* ;

public class DemoClass {
    public static void main(String[] args) throws Exception {
        String url="jdbc:mysql://localhost:3306//crudapi?autoReconnect=true&useSSL=false";
        String uname="root";
        String pass="root";
        String query="select name from tbl_employee where id=1";
        Connection con= DriverManager.getConnection(url,uname,pass);
        Statement st=con.createStatement();
        ResultSet rs=st.executeQuery(query);

        rs.next();
        String name=rs.getString("username");
        System.out.println(name);

        st.close();
        con.close();




    }
}
