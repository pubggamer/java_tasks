package springboot.springbootcrudapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.springbootcrudapi.Repository.EmployeeRepo;
import springboot.springbootcrudapi.dao.EmployeeDAO;
import springboot.springbootcrudapi.exception.ResourceNotFoundException;
import springboot.springbootcrudapi.model.Employee;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepo employeeRepo;

    @Override
    public List<Employee> getAllEmployees() {
        return employeeRepo.findAll();
    }

    @Override
    public Employee createEmployee(Employee employee) {
        return employeeRepo.save(employee);
    }

    @Override
    public Employee getEmployeeById(Integer empid) {
        return employeeRepo.findById(empid)
                .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", empid));
    }

    @Override
    public Employee updateEmployee(Integer empid, Employee empDetails) {
        Employee employee = employeeRepo.findById(empid)
                .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", empid));

        employee.setName(empDetails.getName());
        employee.setGender(empDetails.getGender());
        employee.setDepartment(empDetails.getDepartment());

        Employee updatedEmp = employeeRepo.save(employee);
        return updatedEmp;
    }

    @Override
    public ResponseEntity<?> deleteEmployee(Integer empid) {
        Employee employee = employeeRepo.findById(empid)
                .orElseThrow( ()-> new ResourceNotFoundException("Employee", "id", empid) );

        employeeRepo.delete(employee);
        return ResponseEntity.ok().build();
    }
}
