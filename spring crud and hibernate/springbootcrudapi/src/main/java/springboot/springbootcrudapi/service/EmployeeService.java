package springboot.springbootcrudapi.service;

import org.springframework.http.ResponseEntity;
import springboot.springbootcrudapi.model.Employee;

import java.util.List;

public interface EmployeeService {

    public List<Employee> getAllEmployees();

    public Employee createEmployee(Employee employee);

    public Employee getEmployeeById(Integer eid);

    public Employee updateEmployee(Integer eid, Employee empDetails);

    public ResponseEntity<?> deleteEmployee(Integer eid);
}
