package springboot.springbootcrudapi.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import springboot.springbootcrudapi.model.Employee;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Integer> {
}
