package springboot.springbootcrudapi.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import springboot.springbootcrudapi.model.Employee;
import springboot.springbootcrudapi.service.EmployeeService;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employee")
    public List<Employee> get(){
        return employeeService.getAllEmployees();

    }
    @PostMapping(path = "/post")
    public Employee save(@RequestBody Employee employeeObj){
        System.out.println(employeeObj);
        employeeService.createEmployee(employeeObj);
        return employeeObj;
    }
    @GetMapping("/employee/{id}")
    public Employee get(@PathVariable(value = "id") Integer id)
    {
        Employee employeeObj=employeeService.getEmployeeById(id);
        if(employeeObj==null){
            throw new RuntimeException("Employee with id:"+id+"is not found");
        }
        return employeeObj;

    }
    @DeleteMapping("/employee/{id}")
    public String delete(@PathVariable(value = "id") Integer id){
        employeeService.deleteEmployee(id);
        return "Employee has been deleted with id:"+id;
    }
    @PutMapping("/employee/{id}")
    public Employee update(@PathVariable(value = "id") Integer id, @RequestBody Employee employeeObj){

        employeeService.updateEmployee(id, employeeObj);
        return employeeObj;
    }


}
